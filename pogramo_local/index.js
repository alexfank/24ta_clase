const {Sequelize} = require('sequelize');

const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

//  MOSTRAR USUARIOS
sequelize.query('SELECT * FROM usuarios',
    {type:sequelize.QueryTypes.SELECT})
    .then(function(resultados){
        console.log(resultados)
    });

//  MOSTRAR USUARIOS
sequelize.query('SELECT * FROM usuarios WHERE nombre =:nombre',
    {replacements:{nombre:'Pedro'},type:sequelize.QueryTypes.SELECT})
    .then(function(resultados){
        console.log(resultados)
    });