const express = require('express');
const app = express();

//  Llamamos a la BD
require('./db');

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.listen(3000,() => {
    console.log('Servidor ejecutándose por el puerto 3000')
});