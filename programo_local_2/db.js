const { Sequelize } = require('sequelize');
const PeliculaModelo = require('./models/pelicula');

// Configuración de base de datos sequelize
const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

validar_conexion();

const Pelicula = PeliculaModelo(sequelize,Sequelize);

sequelize.sync({force:false})
    .then(() => {
        console.log('tablas sincronizadas');
    })


async function validar_conexion(){
    try {
        await sequelize.authenticate();
        console.log('La conexión ha sido extablecida correctamente');
    } catch (error) {
        console.log('Error al conectarse a la base de datos: ', error);
    }
};

module.exports = {
    Pelicula,
};